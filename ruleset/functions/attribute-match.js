
function test(value, regex) {
  
//function test(value: string, regex: RegExp | string) {
  let re;
  if (typeof regex === 'string') {
    // regex in a string like {"match": "/[a-b]+/im"} or {"match": "[a-b]+"} in a json ruleset
    // the available flags are "gimsuy" as described here: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp
    const splitRegex = /^\/(.+)\/([a-z]*)$/.exec(regex);
    if (splitRegex) {
      // with slashes like /[a-b]+/ and possibly with flags like /[a-b]+/im
      re = new RegExp(splitRegex[1], splitRegex[2]);
    } else {
      // without slashes like [a-b]+
      re = new RegExp(regex);
    }
  } else {
    // true regex
    re = new RegExp(regex);
  }
  return re.test(value);
}


module.exports = (targetVal, opts, paths) => {
    const { notMatch } = opts;
    console.log('xxx::' + '::xxxtargetVal::' + JSON.stringify(targetVal) );
    console.log('xxx::' + '::::::xxxpaths::'  + targetVal + '....' + JSON.stringify(paths) );
    if (notMatch && Array.isArray(paths)) {

      if (test(paths.slice(-1), notMatch) === true) {
        return [
          {
            message: 'Attribute {' + paths.slice(-1) + '} must not match '
          },
        ];
      }
    }
  };