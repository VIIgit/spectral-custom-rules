
module.exports = (targetVal, opts, paths) => {
  if (Array.isArray(targetVal)) {
    return;
  }

  const { pathPattern, expectedResponseCode, httpVerbs } = opts;
  const rootPath = paths.target !== void 0 ? paths.target : paths.given;
  const results = [];

  for (var prop in targetVal) {
    if (prop.match(pathPattern)) {

      const methods = targetVal[prop];
      for (var methodAttrib in methods) {

        if (httpVerbs.indexOf(methodAttrib) > -1) {

          const method = methods[methodAttrib];

          if (expectedResponseCode && method.responses && !method.responses[expectedResponseCode]) {

            results.push({
              message: `Missing response HTTP Code '${expectedResponseCode}'`,
              path: [...rootPath, prop, methodAttrib]
            });
          }
        }
      }
    }
  }

  return results;
};