const fs = require('fs');
const { join } = require('path');
const ruleFileExtension = '.yaml';
const rulesetFolder = './ruleset/';
const yaml = require('js-yaml');
const readYaml = (fileName) => yaml.safeLoad(fs.readFileSync(fileName, 'utf8'));

const { Spectral, isOpenApiv3, isOpenApiv2 } = require('@stoplight/spectral');
const { parse } = require('url');
const spectral = new Spectral();
spectral.registerFormat('oas2', isOpenApiv2);
spectral.registerFormat('oas3', isOpenApiv3);

fs.readdirSync(`${rulesetFolder}`).map((fileName) => {

  if (fileName.startsWith('.') || !fileName.endsWith(ruleFileExtension)) {
    return;
  }

  const rule = fileName.substr(0, fileName.length - ruleFileExtension.length);
  const openapi = readYaml(`./test/${rule}/api.yaml`);

  test(rule, () => spectral.loadRuleset(join( __dirname, `.${rulesetFolder}${fileName}`))
    .then(() => spectral.run(openapi))
    .then((result) => {
      const expectedResult = `./test/${rule}/expected-result.json`;
      fs.access(expectedResult, fs.F_OK, (err) => {
        if (err) {
          let resultJson = JSON.stringify(result, null, 2);
          fs.writeFile(expectedResult, resultJson, function (err) {
            if (err) throw err;
            console.log(expectedResult + ' created');
          });
        }
      });
      expect(result).toEqual(readYaml(expectedResult));
    })
  );
});